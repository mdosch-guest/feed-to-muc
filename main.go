/* Copyright Martin Dosch
Licensed under the "MIT License" */

package main

import (
	"flag"
	"log"
	"time"

	"github.com/xmppo/go-xmpp"
)

type configuration struct {
	ServerAddress string
	BotJid        string
	Password      string
	Muc           string
	MucNick       string
	MaxArticles   int
	RefreshTime   time.Duration
	NoExcerpt     bool
	Quiet         bool
	PLAIN         bool
	Contact       string
	Filter        []string
	FilterMessage []string
	Feeds         []string
}

// Variables defined globally as they are used by functions pingMUC
// and processStanzas.
var (
	ID           string
	pingSent     time.Time
	pingReceived bool
)

func main() {
	// Read path to config from command line option.
	configFilePtr := flag.String("config", "none", "path to configuration file")
	debugPtr := flag.Bool("debug", false, "debug output")
	flag.Parse()

	config := openConfig(configFilePtr)

	var client *xmpp.Client

	options := xmpp.Options{
		Host:        config.ServerAddress,
		User:        config.BotJid,
		Password:    config.Password,
		NoTLS:       true,
		StartTLS:    true,
		Debug:       *debugPtr,
		SSDP:        true,
		NoPLAIN:     !config.PLAIN,
		UserAgentSW: "feed-to-muc",
	}

	// Connect to server.
	client, err := options.NewClient()
	if err != nil {
		log.Fatal("Error: Can't connect to xmpp server: ", err)
	}

	// Join the MUC
	_, err = client.JoinMUCNoHistory(config.Muc, config.MucNick)
	if err != nil {
		log.Fatal("Error: Can't join MUC: ", err)
	}

	// Starting goroutine to ping the server every 30 seconds.
	go pingServer(client, config.ServerAddress, config.BotJid)

	// Starting goroutine to ping the MUC every 30 seconds.
	go pingMUC(client, config.BotJid, config.Muc, config.MucNick)

	// Starting goroutine to process received stanzas.
	go processStanzas(client, config.Muc, config.MucNick, config.Feeds, config.Quiet, config.Contact)

	// Set RefreshTime to 30 seconds if not defined.
	if config.RefreshTime == 0 {
		config.RefreshTime = 30
	}

	for {
		// Check all configured feeds for new articles and send
		// new articles to configured MUC.
		for _, confFeed := range config.Feeds {
			output, err := getArticles(confFeed, config.MaxArticles, config.NoExcerpt, config.Filter, config.FilterMessage)
			if err != nil {
				// Exit if an error occurs checking the feeds.
				log.Fatal("Error: Can't check feeds for new articles: ", err)
			}

			if output != "" {
				_, err = client.Send(xmpp.Chat{
					Remote: config.Muc,
					Type:   "groupchat", Text: output,
				})
				if err != nil {
					// ToDo: Save message for resend.
					// Exit if message can not be sent.
					log.Fatal("Error: Can't send message to MUC: ", err)
				}
			}
		}
		// Wait 30 seconds before checking feeds again.
		time.Sleep(config.RefreshTime * time.Second)
	}
}
