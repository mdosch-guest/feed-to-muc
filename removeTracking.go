/* Copyright Martin Dosch
Licensed under the "MIT License" */

package main

import (
	"net/http"
	"strings"
)

// Remove tracking parameter from URLs
func removeTracking(input string) (output string, err error) {
	// Perform a get request to get rid of 301 forwarding through
	// services like feedproxy.google.com.
	resp, err := http.Get(input)
	if err != nil {
		return input, err
	}

	output = resp.Request.URL.String()

	// Remove the URL part starting with known tracking parameters.
	trackingParameters := [6]string{"?utm_", "?wt_", "#ref=", "?ref=", "?pk_", "?source="}

	for _, trackingParameter := range trackingParameters {
		output = strings.Split(output, trackingParameter)[0]
	}

	return output, err
}
