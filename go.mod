module salsa.debian.org/mdosch/feed-to-muc

go 1.23.0

toolchain go1.23.5

require (
	github.com/chilts/sid v0.0.0-20190607042430-660e94789ec9
	github.com/jaytaylor/html2text v0.0.0-20230321000545-74c2419ad056
	github.com/mmcdole/gofeed v1.3.0
	github.com/xmppo/go-xmpp v0.2.11-0.20250318194617-c9aeffea064b
)

require (
	github.com/PuerkitoBio/goquery v1.10.2 // indirect
	github.com/andybalholm/cascadia v1.3.3 // indirect
	github.com/json-iterator/go v1.1.12 // indirect
	github.com/mattn/go-runewidth v0.0.16 // indirect
	github.com/mmcdole/goxpp v1.1.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	github.com/rivo/uniseg v0.4.7 // indirect
	github.com/ssor/bom v0.0.0-20170718123548-6386211fdfcf // indirect
	golang.org/x/crypto v0.36.0 // indirect
	golang.org/x/net v0.37.0 // indirect
	golang.org/x/text v0.23.0 // indirect
)
