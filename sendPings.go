/* Copyright Martin Dosch
Licensed under the "MIT License" */

package main

import (
	"log"
	"strings"
	"time"

	"github.com/chilts/sid"
	"github.com/xmppo/go-xmpp"
)

// Send a ping every 30 seconds after last successful ping to check if the MUC is still available.
func pingMUC(client *xmpp.Client, botJid string, muc string, mucNick string) {
	var err error

	for {
		time.Sleep(30 * time.Second)

		// Send ping to own MUC participant to check we are still joined.
		ID, err = client.RawInformation(botJid, muc+"/"+mucNick, sid.Id(),
			"get", "<ping xmlns='urn:xmpp:ping'/>")
		if err != nil {
			log.Fatal("Error: Can't send MUC self ping: ", err)
		}

		pingSent = time.Now()
		pingReceived = false

		// Check for result IQ as long as there was no reply yet.
		for (time.Since(pingSent).Seconds() < 10.0) && !pingReceived {
			time.Sleep(1 * time.Second)
			if pingReceived {
				break
			}

		}
		// Quit if no ping reply was received.
		if !pingReceived {
			log.Fatal("MUC not available. pingMUC")
		}
	}
}

// Send a ping to the server every 30 seconds to check if the connection is still alive.
func pingServer(client *xmpp.Client, server string, botJid string) {
	// Strip port
	server = strings.Split(server, ":")[0]
	// Use JIDs domainpart if ServerAddress is not configured.
	if server == "" {
		server = strings.Split(botJid, "@")[1]
	}
	for {
		time.Sleep(30 * time.Second)

		// Send ping to server to check if connection still exists.
		err := client.PingC2S(botJid, server)
		if err != nil {
			log.Fatal("Error: Client2Server ping failed:", err)
		}
	}
}
