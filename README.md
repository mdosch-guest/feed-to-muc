# feed-to-muc

## about

**feed-to-muc is no more actively developed.** It works ok enough and
I am not interested to add more features or fix corner cases where
it might fail. If you're looking for an actively developed alternative
you might want to have a look at [Morbot](https://codeberg.org/TheCoffeMaker/Morbot).

*feed-to-muc* is an XMPP bot which queries Atom or RSS newsfeeds and
posts the short summary to a XMPP MUC if there is a new article.

## disclaimer

I am no programmer and this bot is a result of a lot of *try and error*.
There are probably lots of quirks that are remains of some wrong paths
I went in between. Also a lot is probably solved in a *hacky way* due
to missing experience.

Anyway, it works (for me at least) and so I upload this here.
Recommendations about what can be done better improved and so on are
very welcome.

## requirements

* [go](https://golang.org/) >= 1.18

## installation

You will find the binary in `$GOPATH/bin` or, if set, `$GOBIN`.

```plain
$ go install salsa.debian.org/mdosch/feed-to-muc@latest
```

## configuration

If the flag `-config` is not used the configuration is expected at
`$HOME/.config/feed-to-muc/config.json` in this format:

```json
{
"ServerAddress": "example.com:5222",
"BotJid":        "feedbot@example.com",
"Password":      "ChangeThis!",
"Muc":           "muc-to-feed@conference.example.com",
"MucNick":       "feedbot",
"Contact":       "xmpp:botadmin@example.com",
"MaxArticles":    5,
"RefreshTime":    30,
"NoExcerpt":     false,
"Quiet":         false,
"PLAIN":         false,
"Filter":        [ "submitted by", "[link]" ],
"FilterMessage": [ "Block me!", "Block me too!" ],
"Feeds":         [ "https://www.debian.org/News/news",
                 "https://www.debian.org/security/dsa-long",
                 "https://www.reddit.com/r/FDroidUpdates/new.rss" ]
}
```

`MaxArticles` is the maximum number of articles that are sent per
feed and query. If `NoExcerpt` is set to *true* no excerpt will be
posted. `RefreshTime` defines the interval for checking the feeds
in seconds.

With `Filter` you can specify strings for filtering out lines
beginning with those strings, with `FilterMessage` you can filter
out complete postings containing the string.

`Contact` is for providing contact information about who is running
the bot.

If you don't want additional noise in the MUC you can set `Quiet` 
to disable bot queries (e.g. *contact*) in the MUC (queries per 
private message are still available).
